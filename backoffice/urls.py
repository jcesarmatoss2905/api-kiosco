
from django.urls import path
from .controllers import login,dashboard,parametros,usuarios,empresas,comprobantes,series,auditoria,proceso,acceso

urlpatterns = [
    path('', login.index),
    path('login/validate', login.validate, name='post'),
    path('logout', login.logout, name='post'),
    path('dashboard', dashboard.index),
    path('parametros', parametros.index),
    path('usuarios', usuarios.index),
    path('empresas', empresas.index),
    path('comprobantes', comprobantes.index),
    path('series', series.index),
    path('auditoria', auditoria.index),
    path('proceso', proceso.index),
    #Cambio 1
    path('acceso', acceso.index),
]