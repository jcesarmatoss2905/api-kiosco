from django.shortcuts import render
from django.shortcuts import render
from core.libs import constants
from rest_framework import exceptions

def index(request):
    if not constants.get_session_id() in request.session: raise exceptions.NotAuthenticated()
    return render(request,'dashboard.html',context={"usuario":request.session[constants.get_session_id()]})