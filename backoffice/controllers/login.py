from django.shortcuts import render
from django.shortcuts import render
from core.libs import constants
from django.shortcuts import redirect

import requests

def index(request):
    if constants.get_session_id() in request.session: del request.session[constants.get_session_id()]
    return render(request,'login.html')

def logout(request):
    if constants.get_session_id() in request.session: del request.session[constants.get_session_id()]
    return redirect('/')

def validate(request):
    args = {}
    args['from_validation'] = True
    
    parameters = {"Usuario":request.POST["user"], "Clave":request.POST["password"]}
    
    response = requests.post(url=constants.get_api_url('usuarios','validate_user')
                             , headers={'Content-Type': 'application/json; charset=utf-8'}
                             , json=parameters)
    #my_headers = {'Authorization' : 'Bearer {access_token}'} --, headers=my_headers
    response = response.json()
    
    if(response[constants.get_api_response_status()]==constants.get_api_success()):
        request.session[constants.get_session_id()] = response["data"]
        args['autorize'] = True
        args['mensaje'] = response[constants.get_api_response_message()]
    else:
        if constants.get_session_id() in request.session: del request.session[constants.get_session_id()]
        args['autorize'] = False
        args['mensaje'] = response[constants.get_api_response_message()]
        
    return render(request,'login.html',context=args)
