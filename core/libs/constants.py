from django import template
from django.conf import settings
import os

register = template.Library()

@register.simple_tag
def get_url():
    return os.environ.get("HOST", default=settings.DEFAULT_DOMAIN)+"/"

@register.simple_tag
def get_api_url(service,method): return get_url()+"api/"+service+'/'+method+'/'

@register.simple_tag
def get_bo_url(service,method): return get_url()+service+('/' if service!="" and method!="" else '')+method


@register.simple_tag
def get_api_response_status(): return "status"
@register.simple_tag
def get_api_response_data(): return "data"
@register.simple_tag
def get_api_response_message(): return "message"

@register.simple_tag
def get_api_success(): return "success"
@register.simple_tag
def get_api_error(): return "error"

@register.simple_tag
def get_session_id(): return "usuario"


@register.simple_tag
def set_value(value): return value
@register.simple_tag
def set_value_page(value): return value.replace("/","").lower()

@register.simple_tag
def get_state_pending(): return "PENDING"
@register.simple_tag
def get_state_success(): return "SUCCESS"
@register.simple_tag
def get_state_fail(): return "FAIL"
@register.simple_tag
def get_state_error(): return "ERROR"


@register.simple_tag
def get_source_email(): return "EMAIL"
@register.simple_tag
def get_source_service(): return "SERVICE"
@register.simple_tag
def get_source_sftp(): return "SFTP"