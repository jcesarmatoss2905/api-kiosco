import os
import email
import imaplib, getpass, re
from email import utils

def get_body(tmsg):    
    body = ""
    if tmsg.is_multipart():        
        for part in tmsg.walk():
            ctype = part.get_content_type()
            cdispo = str(part.get('Content-Disposition'))    
            # skip any text/plain (txt) attachments
            if ctype == 'text/plain' and 'attachment' not in cdispo:
                body = str(part.get_payload(decode=True))  # decode
                body=body.replace("\\r\\n","\n")                
                break
    # not multipart - i.e. plain text, no attachments, keeping fingers crossed
    else:
        body = str(tmsg.get_payload(decode=True))
    return body

def get_emailinfo(id, data, bshowbody=False):
    for contenido in data:
        # comprueba que 'contenido' sea una tupla, si es así continua
        if isinstance(contenido, tuple):                    
            # recuperamos información del email:                    
            msg = email.message_from_string(contenido[1].decode())
            # mostramos resultados:   
            print ("%d - *** %s ***" % (id, msg['subject'].upper()))
            print ("enviado por %s" % msg['from'])
            print ("para %s" % msg['to'])
            if(bshowbody):
                print ("---                                                   ---")
                print(get_body(msg))
                print ("--------------------------------------------------------")
            return True
    # si no hay info
    return False

def get_emails(gmailsmtpsvr, gmailport, gmailusr, gmailpwd, bshowbody, filters,downloadpath):
    try:
        mail = imaplib.IMAP4_SSL(host=gmailsmtpsvr,port=gmailport)
        mail.login(gmailusr, gmailpwd)
        mail.select("inbox")
        # recuperamos lista de emails, es posible filtrar la consulta
        # ALL devuelve todos los emails
        # Ejemplo de filtro: '(FROM "altaruru" SUBJECT "ejemplo python")'        
        result, data = mail.search(None, filters)
        strids = data[0] # coge lista de ids encontrados
        lstids = strids.split()
        # recuperamos valores para bucle
        firstid = int(lstids[0])
        lastid = int(lstids[-1])
        countid = 0
        # mostramos datos de los ids encontrados
        # recorremos lista de mayor a menor (mas recientes primero)
        for id in range(lastid, firstid-1, -1):            
            typ, data = mail.fetch(str(id), '(RFC822)' ) # el parámetro id esperado es tipo cadena
            if (get_emailinfo(id, data, bshowbody)):
                #downloaAttachmentsInEmail(mail,str(id),downloadpath)
                countid+=1
    except Exception as e:
        print("Error: %s" % (e))
        return ""
    except:
        print("Error desconocido")
        return ""
    










from imapclient import IMAPClient
def get_email_connection2(gmailsmtpsvr, gmailport, gmailusr, gmailpwd):
    try:
        mail = IMAPClient(gmailsmtpsvr,use_uid=True)
        mail.login(gmailusr, gmailpwd)
        mail.select_folder("inbox")
        return mail
    except Exception as e:
        raise e
def isMailForProcess2(msg):
    part_file_pdf = None
    part_file_xml = None
    part_file_cdr = None
    
    xml_file_name = None
    email_from = msg.get("From")
    email_received_at = utils.parsedate_to_datetime(msg.get("Date"))
    email_subject = msg.get("Subject")
    
    for part in msg.walk():
        if part.get_content_maintype() == 'multipart': continue
        if part.get('Content-Disposition') is None: continue
        file_name, file_extension = os.path.splitext(part.get_filename())
        if(file_extension.lower() == ".pdf" and part_file_pdf==None):
            r = re.compile('.*-.*-.*-.*')
            if(r.match(file_name)):
                part_file_pdf=part
        elif(file_extension.lower() == ".xml" and part_file_xml==None and "CDR-" not in file_name and "R-" not in file_name):
            r = re.compile('.*-.*-.*-.*')
            if(r.match(file_name)):
                part_file_xml=part
                xml_file_name=file_name
        elif(file_extension.lower() == ".xml" and part_file_cdr==None):
            r = re.compile('R-.*-.*-.*-.*')
            r2 = re.compile('CDR-.*-.*-.*-.*')
            if(r.match(file_name) or r2.match(file_name)):
                part_file_cdr=part
    
    ruc_provider = None
    if(xml_file_name is not None
       and part_file_pdf is not None
       and part_file_xml is not None
       and part_file_cdr is not None):
        ruc_provider=xml_file_name.split("-")[0]
        return [ruc_provider,[part_file_pdf,part_file_xml,part_file_cdr],[{"ruc":ruc_provider,"from":email_from,"received_at":email_received_at,"subject":email_subject,"file_name":xml_file_name,"file":part_file_xml}]]
    return None
def isMailForValidForProcess(msg):
    xml_files = []
    
    email_from = msg.get("From")
    email_received_at = utils.parsedate_to_datetime(msg.get("Date"))
    email_subject = msg.get("Subject")
    
    #PRIMERA VALIDACIÓN DE ARCHIVOS (FORMADO POR 3 ARCHIVOS PDF, XML Y CDR)
    part_file_pdf = None
    part_file_xml = None
    part_file_cdr = None
    for part in msg.walk():
        if part.get_content_maintype() == 'multipart': continue
        if part.get('Content-Disposition') is None: continue
        file_name, file_extension = os.path.splitext(part.get_filename())
        if(file_extension.lower() == ".pdf" and part_file_pdf==None):
            r = re.compile('.*-.*-.*-.*')
            if(r.match(file_name)):
                part_file_pdf=part
        elif(file_extension.lower() == ".xml" and part_file_xml==None and "CDR-" not in file_name and "R-" not in file_name):
            r = re.compile('.*-.*-.*-.*')
            if(r.match(file_name)):
                part_file_xml=part
        elif(file_extension.lower() == ".xml" and part_file_cdr==None):
            r = re.compile('R-.*-.*-.*-.*')
            r2 = re.compile('CDR-.*-.*-.*-.*')
            if(r.match(file_name) or r2.match(file_name)):
                part_file_cdr=part
    
    if(part_file_pdf is not None
       and part_file_xml is not None
       and part_file_cdr is not None):
        xml_files.append(part_file_xml)
    
    if(len(xml_files)==0):
        #SI NO SE ENCONTRÓ EL PATRON ANTERIOR ENTONCES BUSCAMOS MULTIXML
        for part in msg.walk():
            if part.get_content_maintype() == 'multipart': continue
            if part.get('Content-Disposition') is None: continue
            file_name, file_extension = os.path.splitext(part.get_filename())
            if(file_extension.lower() == ".xml"):
                xml_files.append(part)
    
    xml_return = []
    ruc_provider = None
    for xml in xml_files:
        filename,ext = os.path.splitext(xml.get_filename())
        ruc_provider=filename[:11]
        if(ruc_provider.isdigit() and (ruc_provider[:2]=="10" or ruc_provider[:2]=="20")):
            xml_return.append({"attach":xml,"filename":filename,"fileext":ext,"ruc":ruc_provider,"from":email_from,"received_at":email_received_at,"subject":email_subject})
    return xml_return

def downloaAttachmentsInEmail2(msg,save_path):
    result = isMailForProcess2(msg)
    ruc_of_provider = result[0]
    download_parts = result[1]
    
    if(ruc_of_provider is not None):
        for part in download_parts:
            validateAndCreateFolder(save_path + '/' + ruc_of_provider)
            open(save_path + '/' + ruc_of_provider + '/' + part.get_filename(), 'wb').write(part.get_payload(decode=True))
          
def downloadXMLForProcess(ruc,xml_file,save_path):
    validateAndCreateFolder(save_path + '/' + ruc)
    open(save_path + '/' + ruc + '/' + xml_file.get_filename(), 'wb').write(xml_file.get_payload(decode=True))
            


  























def get_email_connection(gmailsmtpsvr, gmailport, gmailusr, gmailpwd):
    try:
        mail = imaplib.IMAP4_SSL(host=gmailsmtpsvr,port=gmailport)
        mail.login(gmailusr, gmailpwd)
        mail.select("inbox")
        return mail
    except Exception as e:
        raise e
def get_emails_id(mail, filters):
    lists_ids = []
    try:
        # recuperamos lista de emails, es posible filtrar la consulta
        # ALL devuelve todos los emails
        # Ejemplo de filtro: '(FROM "altaruru" SUBJECT "ejemplo python")'        
        result, data = mail.search(None, filters)
        strids = data[0] # coge lista de ids encontrados
        lstids = strids.split()
        # recuperamos valores para bucle
        firstid = int(lstids[0])
        lastid = int(lstids[10])
        countid = 0
        for id in range(lastid, firstid-1, -1):
            typ, data = mail.fetch(str(id), '(RFC822)' ) # el parámetro id esperado es tipo cadena
            lists_ids.append(str(id))
            countid+=1
        return lists_ids
    except Exception as e:
        print("Error: %s" % (e))
        return None
    except:
        print("Error desconocido")
        return None
def isMailForProcess(m, emailid):
    part_file_xml = None
    email_from = ""
    email_received_at = ""
    email_subject = ""
    
    resp, data = m.fetch(emailid, "(BODY.PEEK[])")
    email_body = data[0][1]
    mail = email.message_from_bytes(email_body)
    if mail.get_content_maintype() != 'multipart':
        return None
    
    res, msg = m.fetch(emailid, "(RFC822)")  
    for response in msg:
        if isinstance(response, tuple):
            msg = email.message_from_bytes(response[1])
            email_from = msg["From"]
            email_received_at = utils.parsedate_to_datetime(msg['date'])
            email_subject = msg["Subject"]
    
    xml_file_name = None
    for part in mail.walk():
        if part.get_content_maintype() != 'multipart' and part.get('Content-Disposition') is not None:
            file_name, file_extension = os.path.splitext(part.get_filename())
            if(file_extension.lower() == ".xml" and part_file_xml==None and "CDR" not in file_name and "R" not in file_name):
                r = re.compile('.*-.*-.*-.*')
                if(r.match(file_name)):
                    part_file_xml=part
                    xml_file_name=part.get_filename();
    
    ruc_provider = None
    if(part_file_xml is not None):
        print("SI TIENE FORMATO")
        ruc_provider=xml_file_name.split("-")[0]
        return {"ruc":ruc_provider,"from":email_from,"received_at":email_received_at,"subject":email_subject,"file_name":xml_file_name,"file":part_file_xml}
    return None
def getFilesForDownloadFull(mail):
    part_file_pdf = None
    part_file_xml = None
    part_file_cdr = None
    
    pdf_file_name = None
    for part in mail.walk():
        if part.get_content_maintype() != 'multipart' and part.get('Content-Disposition') is not None:
            file_name, file_extension = os.path.splitext(part.get_filename())
            
            if(file_extension.lower() == ".pdf" and part_file_pdf==None):
                r = re.compile('.*-.*-.*-.*')
                if(r.match(file_name)):
                    part_file_pdf=part
                    pdf_file_name=file_name;
            elif(file_extension.lower() == ".xml" and part_file_xml==None and "CDR" not in file_name):
                r = re.compile('.*-.*-.*-.*')
                if(r.match(file_name)):
                    part_file_xml=part
            elif(file_extension.lower() == ".xml" and part_file_cdr==None):
                r = re.compile('CDR-.*-.*-.*-.*')
                if(r.match(file_name)):
                    part_file_cdr=part
    
    ruc_provider = None
    if(pdf_file_name is not None
       and part_file_pdf is not None
       and part_file_xml is not None
       and part_file_cdr is not None):
        ruc_provider=pdf_file_name.split("-")[0]
    
    return ruc_provider,[part_file_pdf,part_file_xml,part_file_cdr]

def downloaAttachmentsInEmail(m, emailid, outputdir):
    resp, data = m.fetch(emailid, "(BODY.PEEK[])")
    email_body = data[0][1]
    mail = email.message_from_bytes(email_body)
    if mail.get_content_maintype() != 'multipart':
        return
    
    ruc_of_provider,download_parts = getFilesForDownloadFull(mail)
    
    if(ruc_of_provider is not None):
        for part in download_parts:
            validateAndCreateFolder(outputdir + '/' + ruc_of_provider)
            open(outputdir + '/' + ruc_of_provider + '/' + part.get_filename(), 'wb').write(part.get_payload(decode=True))
            
def validateAndCreateFolder(download_dir):
    if not os.path.exists(download_dir):
        os.makedirs(download_dir)
        print("The new directory is created: "+download_dir)
