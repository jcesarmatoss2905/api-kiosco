
from xml.dom import minidom
import xmltodict
import json

def getChildrenByTagName(node, tagName):
    for child in node.childNodes:
        if child.nodeType==child.ELEMENT_NODE and (tagName=='*' or child.tagName==tagName):
            return child
    return None

def getUniqueXML(parent,identifier,get_data=False):
    try:
        identifiers = identifier.split("|")
        if(len(identifiers)==1):
            if(get_data):
                return getChildrenByTagName(parent,identifiers[0]).firstChild.data
            else:
                return getChildrenByTagName(parent,identifiers[0])
        else:
            newidentifier=""
            for x in range(1,len(identifiers)):
                if((x==len(identifiers)-1)):
                    newidentifier=newidentifier+identifiers[x]
                else:
                    newidentifier=newidentifier+identifiers[x]+'|'
            new_parent = getChildrenByTagName(parent,identifiers[0])
            return getUniqueXML(new_parent,newidentifier,get_data)
    except Exception as e:
        return None
    
def getXMLValuesForRegister(route): 
    print(route)
    try:
        doc = minidom.parse(route)

        toInsert = []
        invoices = doc.getElementsByTagName("Invoice")
        
        for inv in invoices:
            version = getUniqueXML(inv,'cbc:UBLVersionID',True)
            if(version=="2.1"):
                lines = getDataVersion_2dot1(inv,route)
            if(version=="2.0"):
                lines = getDataVersion_2dot0(inv,route)
                
            if(lines is None): return None
            for l in lines: toInsert.append(l)

        return toInsert
    except Exception as e:
        print("Archivo corrupto o sin formato correcto")
        return None
        
def getDataVersion_2dot1(inv,route):
    try:
        toReturn = []
        line = dict((k, []) for k in range(0))
        
        line["CodigoMall"]=None
        line["CodigoTienda"]=None
        line["RucEmisor"]=(getUniqueXML(inv,'cac:AccountingSupplierParty|cac:Party|cac:PartyIdentification|cbc:ID',True))
        line["IdentificadorTerminal"]=None
        line["NumeroTerminal"]=None
        line["Serie"]=(getUniqueXML(inv,'cbc:ID',True)).split("-")[0]
        line["TipoTransaccion"]=(getUniqueXML(inv,'cbc:InvoiceTypeCode',True))
        line["NumeroTransaccion"]=(getUniqueXML(inv,'cbc:ID',True)).split("-")[1]
        line["Fecha"]=(getUniqueXML(inv,'cbc:IssueDate',True))
        line["Hora"]=(getUniqueXML(inv,'cbc:IssueTime',True))
        line["Cajero"]=None
        line["Vendedor"]=None
        line["DNICliente"]=(getUniqueXML(inv,'cac:AccountingCustomerParty|cac:Party|cac:PartyIdentification|cbc:ID',True))
        line["RUCCliente"]=(getUniqueXML(inv,'cac:AccountingCustomerParty|cac:Party|cac:PartyIdentification|cbc:ID',True))
        line["NombreRazonSocialCliente"]=(getUniqueXML(inv,'cac:AccountingCustomerParty|cac:Party|cac:PartyLegalEntity|cbc:RegistrationName',True))
        #line["Direccion"]=(getUniqueXML(inv,'cac:AccountingCustomerParty|cac:Party|cac:PartyLegalEntity|cac:RegistrationAddress|cac:AddressLine|cbc:Line',True))
        line["Direccion"]=(getUniqueXML(inv,'cac:AccountingCustomerParty|cac:Party|cac:PartyLegalEntity|cac:RegistrationAddress|cac:AddressLine|cbc:Line',True)) #MODIFICADO POR BITZ
        line["Bonus"]=None
        line["Moneda"]=(getUniqueXML(inv,'cbc:DocumentCurrencyCode',True))
        line["MedioPago"]=None
        line["TotalValorVentaBruta"]=(getUniqueXML(inv,'cac:LegalMonetaryTotal|cbc:TaxInclusiveAmount',True))
        
        ChargeTotalAmount = (getUniqueXML(inv,'cac:LegalMonetaryTotal|cbc:ChargeTotalAmount',True))
        if ChargeTotalAmount is None: ChargeTotalAmount = "0"
        AllowanceTotalAmount = (getUniqueXML(inv,'cac:LegalMonetaryTotal|cbc:AllowanceTotalAmount',True))
        if AllowanceTotalAmount is None: AllowanceTotalAmount = "0"
        line["CargosDescuentosGlobal"]= float(ChargeTotalAmount) - float(AllowanceTotalAmount)
        line["MontoTotalIGV"]=(getUniqueXML(inv,'cac:TaxTotal|cbc:TaxAmount',True))
        line["TotalValorVentaNeta"]=(getUniqueXML(inv,'cac:LegalMonetaryTotal|cbc:LineExtensionAmount',True))
        
        #LINEAS DETALLE (CODIGO DE EXTRACCIÓN MAS ABAJO!)
        
        line["Filename"]=route
        line["CarpetaAzure"]=route
        line["Tienda"]=None
        line["UblType"]="Tag principal del XML"
        line["StatusOSE"]="-"
        line["UBLVersionID"]=getUniqueXML(inv,'cbc:UBLVersionID',True)
        line["CustomizationID"]=(getUniqueXML(inv,'cbc:CustomizationID',True))
        line["ID"]=(getUniqueXML(inv,'cbc:ID',True))
        line["Note"]=(getUniqueXML(inv,'cbc:Note',True))

        line["LineCountNumeric"]=(getUniqueXML(inv,'cac:LegalMonetaryTotal|cbc:LineExtensionAmount',True))
        line["TotalInvoiceAmount"]=(getUniqueXML(inv,'cac:LegalMonetaryTotal|cbc:PayableAmount',True))
        line["SupPartyId"]=(getUniqueXML(inv,'cac:AccountingSupplierParty|cac:Party|cac:PartyIdentification|cbc:ID',True))
        line["SupPartyName"]=(getUniqueXML(inv,'cac:AccountingSupplierParty|cac:Party|cac:PartyName|cbc:Name',True))
        line["SupPartyRegName"]=(getUniqueXML(inv,'cac:AccountingSupplierParty|cac:Party|cac:PartyLegalEntity|cbc:RegistrationName',True))
        line["SupPartyRegAddrId"]=(getUniqueXML(inv,'cac:AccountingSupplierParty|cac:Party|cac:PartyLegalEntity|cac:RegistrationAddress|cbc:ID',True))
        line["SupPartyRegAddrTypeCode"]=(getUniqueXML(inv,'cac:AccountingSupplierParty|cac:Party|cac:PartyLegalEntity|cac:RegistrationAddress|cbc:AddressTypeCode',True))
        line["SupPartyRegAddrLine"]=(getUniqueXML(inv,'cac:AccountingSupplierParty|cac:Party|cac:PartyLegalEntity|cac:RegistrationAddress|cac:AddressLine|cbc:Line',True))
        line["SupPartyRegAddrCountry"]=(getUniqueXML(inv,'cac:AccountingSupplierParty|cac:Party|cac:PartyLegalEntity|cac:RegistrationAddress|cac:Country|cbc:IdentificationCode',True))
        line["CusPartyId"]=(getUniqueXML(inv,'cac:AccountingCustomerParty|cac:Party|cac:PartyIdentification|cbc:ID',True))
        line["CusPartyName"]="-"

        line["CusPartyRegName"]=(getUniqueXML(inv,'cac:AccountingCustomerParty|cac:Party|cac:PartyLegalEntity|cbc:RegistrationName',True))
        line["CusPartyRegAddrId"]=(getUniqueXML(inv,'cac:AccountingCustomerParty|cac:Party|cac:PartyLegalEntity|cac:RegistrationAddress|cbc:ID',True))
        line["CusPartyRegAddrTypeCode"]="-"
        line["CusPartyRegAddrLine"]=(getUniqueXML(inv,'cac:AccountingCustomerParty|cac:Party|cac:PartyLegalEntity|cac:RegistrationAddress|cac:AddressLine|cbc:Line',True))
        line["CusPartyRegAddrCountry"]=(getUniqueXML(inv,'cac:AccountingCustomerParty|cac:Party|cac:PartyLegalEntity|cac:RegistrationAddress|cac:Country|cbc:IdentificationCode',True))
        line["CpbtLines"]=len(inv.getElementsByTagName("cac:InvoiceLine"))
        line["CpbtTotal"]=line["TotalValorVentaNeta"]
        line["matchPos"]=None
        line["FechaCarga"]=None
        line["NombreMall"]=None
        line["NombreTienda"]=None
        
        line["ReferenceID"]=(getUniqueXML(inv,'cac:DiscrepancyResponse|cbc:ReferenceID',True))
        line["ResponseCode"]=(getUniqueXML(inv,'cac:DiscrepancyResponse|cbc:ResponseCode',True))
        line["Description"]=(getUniqueXML(inv,'cac:DiscrepancyResponse|cbc:Description',True))
        line["DocumentTypeCode"]=(getUniqueXML(inv,'cac:BillingReference|cac:InvoiceDocumentReference|cbc:DocumentTypeCode',True))
        line["Cus_party_subdiv_name"]="-"

        line["Cus_party_city_name"]=(getUniqueXML(inv,'cac:AccountingCustomerParty|cac:Party|cac:PartyLegalEntity|cac:RegistrationAddress|cbc:CityName',True))
        line["Cus_party_country_subentity"]=(getUniqueXML(inv,'cac:AccountingCustomerParty|cac:Party|cac:PartyLegalEntity|cac:RegistrationAddress|cbc:CountrySubentity',True))
        line["Cus_party_district"]=(getUniqueXML(inv,'cac:AccountingCustomerParty|cac:Party|cac:PartyLegalEntity|cac:RegistrationAddress|cbc:District',True))
        line["Cus_party_email"]=(getUniqueXML(inv,'cac:AccountingCustomerParty|cac:Party|cac:Person|cac:Contact|cbc:ElectronicMail',True))
        line["Sup_party_subdiv_name"]=(getUniqueXML(inv,'cac:AccountingSupplierParty|cac:Party|cac:PartyLegalEntity|cac:RegistrationAddress|cbc:CitySubdivisionName',True))
        line["Sup_party_city_name"]=(getUniqueXML(inv,'cac:AccountingSupplierParty|cac:Party|cac:PartyLegalEntity|cac:RegistrationAddress|cbc:CityName',True))
        line["Sup_party_country_subentity"]=(getUniqueXML(inv,'cac:AccountingSupplierParty|cac:Party|cac:PartyLegalEntity|cac:RegistrationAddress|cbc:CountrySubentity',True))
        line["Sup_party_district"]=(getUniqueXML(inv,'cac:AccountingSupplierParty|cac:Party|cac:PartyLegalEntity|cac:RegistrationAddress|cbc:District',True))
        line["Sup_party_email"]="-"

        line["PayableRoundingAmount"]=(getUniqueXML(inv,'cac:LegalMonetaryTotal|cbc:PayableRoundingAmount',True))
        line["Cus_party_person_id"]=(getUniqueXML(inv,'cac:AccountingCustomerParty|cac:Party|cac:Person|cac:Contact|cbc:ID',True))
        line["Cus_party_person_email"]=(getUniqueXML(inv,'cac:AccountingCustomerParty|cac:Party|cac:Person|cac:Contact|cbc:ElectronicMail',True))
        line["SignatureID"]=(getUniqueXML(inv,'cac:Signature|cbc:ID',True))
        line["ChargeIndicator"]=(getUniqueXML(inv,'cac:AllowanceCharge|cbc:ChargeIndicator',True))
        line["AllowanceChargeReasonCode"]=(getUniqueXML(inv,'cac:AllowanceCharge|cbc:AllowanceChargeReasonCode',True))
        line["MultiplierFactorNumeric"]=(getUniqueXML(inv,'cac:AllowanceCharge|cbc:MultiplierFactorNumeric',True))
        line["AllowanceChargeAmount"]=(getUniqueXML(inv,'cac:AllowanceCharge|cbc:Amount',True))
        line["BaseAmount"]=(getUniqueXML(inv,'cac:AllowanceCharge|cbc:BaseAmount',True))      

        countTaxes = 1
        invTaxes = inv.getElementsByTagName("cac:TaxTotal")
        for tax in invTaxes:
            if(countTaxes == 1):
                line["SubTotalTaxAmount_01"]=(getUniqueXML(tax,'cac:TaxSubtotal|cbc:TaxAmount',True))
                line["SubTotalTaxCategoryID_01"]=(getUniqueXML(tax,'cac:TaxSubtotal|cac:TaxCategory|cbc:ID',True))
                line["SubTotalTaxID_01"]=(getUniqueXML(tax,'cac:TaxSubtotal|cac:TaxCategory|cac:TaxScheme|cbc:ID',True))
                line["SubTotalTaxName_01"]=(getUniqueXML(tax,'cac:TaxSubtotal|cac:TaxCategory|cac:TaxScheme|cbc:Name',True))
                line["SubTotalTaxTypeCode_01"]=(getUniqueXML(tax,'cac:TaxSubtotal|cac:TaxCategory|cac:TaxScheme|cbc:TaxTypeCode',True))
            elif(countTaxes == 2):
                line["SubTotalTaxAmount_02"]=(getUniqueXML(tax,'cac:TaxSubtotal|cbc:TaxAmount',True))
                line["SubTotalTaxCategoryID_02"]=(getUniqueXML(tax,'cac:TaxSubtotal|cac:TaxCategory|cbc:ID',True))
                line["SubTotalTaxID_02"]=(getUniqueXML(tax,'cac:TaxSubtotal|cac:TaxCategory|cac:TaxScheme|cbc:ID',True))
                line["SubTotalTaxName_02"]=(getUniqueXML(tax,'cac:TaxSubtotal|cac:TaxCategory|cac:TaxScheme|cbc:Name',True))
                line["SubTotalTaxTypeCode_02"]=(getUniqueXML(tax,'cac:TaxSubtotal|cac:TaxCategory|cac:TaxScheme|cbc:TaxTypeCode',True))
            elif(countTaxes == 3):
                line["SubTotalTaxAmount_03"]=(getUniqueXML(tax,'cac:TaxSubtotal|cbc:TaxAmount',True))
                line["SubTotalTaxCategoryID_03"]=(getUniqueXML(tax,'cac:TaxSubtotal|cac:TaxCategory|cbc:ID',True))
                line["SubTotalTaxID_03"]=(getUniqueXML(tax,'cac:TaxSubtotal|cac:TaxCategory|cac:TaxScheme|cbc:ID',True))
                line["SubTotalTaxName_03"]=(getUniqueXML(tax,'cac:TaxSubtotal|cac:TaxCategory|cac:TaxScheme|cbc:Name',True))
                line["SubTotalTaxTypeCode_03"]=(getUniqueXML(tax,'cac:TaxSubtotal|cac:TaxCategory|cac:TaxScheme|cbc:TaxTypeCode',True))
            elif(countTaxes == 4):
                line["SubTotalTaxAmount_04"]=(getUniqueXML(tax,'cac:TaxSubtotal|cbc:TaxAmount',True))
                line["SubTotalTaxCategoryID_04"]=(getUniqueXML(tax,'cac:TaxSubtotal|cac:TaxCategory|cbc:ID',True))
                line["SubTotalTaxID_04"]=(getUniqueXML(tax,'cac:TaxSubtotal|cac:TaxCategory|cac:TaxScheme|cbc:ID',True))
                line["SubTotalTaxName_04"]=(getUniqueXML(tax,'cac:TaxSubtotal|cac:TaxCategory|cac:TaxScheme|cbc:Name',True))
                line["SubTotalTaxTypeCode_04"]=(getUniqueXML(tax,'cac:TaxSubtotal|cac:TaxCategory|cac:TaxScheme|cbc:TaxTypeCode',True))
            countTaxes = countTaxes+1
        
        invline = inv.getElementsByTagName("cac:InvoiceLine")
        for detail in invline:
            
            newlst = line.copy()
            
            newlst["NumeroOrdenItem"]=None
            newlst["CantidadUnidadesItem"]=None
            newlst["CodigoProducto"]=None
            newlst["DescripcionProducto"]=None
            newlst["PrecioVentaUnitarioItem"]=None
            newlst["CargoDescuentoItem"]=None
            newlst["PrecioTotalItem"]=None
            newlst["PrecioVentaUnitarioconIGVItem"]=None
            newlst["PriceTypeCode"]=None
            
            newlst["NumeroOrdenItem"]=(getUniqueXML(detail,'cbc:ID',True))
            newlst["CantidadUnidadesItem"]=(getUniqueXML(detail,'cbc:InvoicedQuantity',True))
            newlst["CodigoProducto"]=(getUniqueXML(detail,'cac:Item|cac:SellersItemIdentification|cbc:ID',True))
            newlst["DescripcionProducto"]=(getUniqueXML(detail,'cac:Item|cbc:Description',True))
            newlst["PrecioVentaUnitarioItem"]=(getUniqueXML(detail,'cac:PricingReference|cac:AlternativeConditionPrice|cbc:PriceAmount',True))
            newlst["CargoDescuentoItem"]=(getUniqueXML(detail,'cac:AllowanceCharge|cbc:Amount',True))
            newlst["PrecioTotalItem"]=(getUniqueXML(detail,'cbc:lineExtensionAmount',True))
            newlst["PrecioVentaUnitarioconIGVItem"]=(getUniqueXML(detail,'cac:PricingReference|cac:AlternativeConditionPrice|cbc:PriceAmount',True))
            newlst["PriceTypeCode"]=(getUniqueXML(detail,'cac:PricingReference|cac:AlternativeConditionPrice|cbc:PriceTypeCode',True))
        
            newlst["SubTotalTaxAmountItem_01"]=None
            newlst["SubTotalTaxCategoryIDItem_01"]=None
            newlst["SubTotalTaxPercentItem_01"]=None
            newlst["SubTotalTaxExemptionReasonCodeItem_01"]=None
            newlst["SubTotalTaxIDItem_01"]=None
            newlst["SubTotalTaxNameItem_01"]=None
            newlst["SubTotalTaxTypeCodeItem_01"]=None
            newlst["SubTotalTaxAmountItem_02"]=None
            newlst["SubTotalTaxCategoryIDItem_02"]=None
            newlst["SubTotalTaxPercentItem_02"]=None
            newlst["SubTotalTaxExemptionReasonCodeItem_02"]=None
            newlst["SubTotalTaxIDItem_02"]=None
            newlst["SubTotalTaxNameItem_02"]=None
            newlst["SubTotalTaxTypeCodeItem_02"]=None
            newlst["SubTotalTaxAmountItem_03"]=None
            newlst["SubTotalTaxCategoryIDItem_03"]=None
            newlst["SubTotalTaxPercentItem_03"]=None
            newlst["SubTotalTaxExemptionReasonCodeItem_03"]=None
            newlst["SubTotalTaxIDItem_03"]=None
            newlst["SubTotalTaxNameItem_03"]=None
            newlst["SubTotalTaxTypeCodeItem_03"]=None
            newlst["SubTotalTaxAmountItem_04"]=None
            newlst["SubTotalTaxCategoryIDItem_04"]=None
            newlst["SubTotalTaxPercentItem_04"]=None
            newlst["SubTotalTaxExemptionReasonCodeItem_04"]=None
            newlst["SubTotalTaxIDItem_04"]=None
            newlst["SubTotalTaxNameItem_04"]=None
            newlst["SubTotalTaxTypeCodeItem_04"]=None
        
            countTaxes = 1
            invTaxes = detail.getElementsByTagName("cac:TaxTotal")
            for tax in invTaxes:
                if(countTaxes == 1):
                    newlst["SubTotalTaxAmountItem_01"]=(getUniqueXML(tax,'cac:TaxSubtotal|cbc:TaxAmount',True))
                    newlst["SubTotalTaxCategoryIDItem_01"]=(getUniqueXML(tax,'cac:TaxSubtotal|cac:TaxCategory|cbc:ID',True))
                    newlst["SubTotalTaxPercentItem_01"]=(getUniqueXML(tax,'cac:TaxSubtotal|cac:TaxCategory|cbc:Percent',True))
                    newlst["SubTotalTaxExemptionReasonCodeItem_01"]=(getUniqueXML(tax,'cac:TaxSubtotal|cac:TaxCategory|cbc:TaxExemptionReasonCode',True))
                    newlst["SubTotalTaxIDItem_01"]=(getUniqueXML(tax,'cac:TaxSubtotal|cac:TaxCategory|cac:TaxScheme|cbc:ID',True))
                    newlst["SubTotalTaxNameItem_01"]=(getUniqueXML(tax,'cac:TaxSubtotal|cac:TaxCategory|cac:TaxScheme|cbc:Name',True))
                    newlst["SubTotalTaxTypeCodeItem_01"]=(getUniqueXML(tax,'cac:TaxSubtotal|cac:TaxCategory|cac:TaxScheme|cbc:TaxTypeCode',True))
                elif(countTaxes == 2):
                    newlst["SubTotalTaxAmountItem_02"]=(getUniqueXML(tax,'cac:TaxSubtotal|cbc:TaxAmount',True))
                    newlst["SubTotalTaxCategoryIDItem_02"]=(getUniqueXML(tax,'cac:TaxSubtotal|cac:TaxCategory|cbc:ID',True))
                    newlst["SubTotalTaxPercentItem_02"]=(getUniqueXML(tax,'cac:TaxSubtotal|cac:TaxCategory|cbc:Percent',True))
                    newlst["SubTotalTaxExemptionReasonCodeItem_02"]=(getUniqueXML(tax,'cac:TaxSubtotal|cac:TaxCategory|cbc:TaxExemptionReasonCode',True))
                    newlst["SubTotalTaxIDItem_02"]=(getUniqueXML(tax,'cac:TaxSubtotal|cac:TaxCategory|cac:TaxScheme|cbc:ID',True))
                    newlst["SubTotalTaxNameItem_02"]=(getUniqueXML(tax,'cac:TaxSubtotal|cac:TaxCategory|cac:TaxScheme|cbc:Name',True))
                    newlst["SubTotalTaxTypeCodeItem_02"]=(getUniqueXML(tax,'cac:TaxSubtotal|cac:TaxCategory|cac:TaxScheme|cbc:TaxTypeCode',True))
                elif(countTaxes == 3):
                    newlst["SubTotalTaxAmountItem_03"]=(getUniqueXML(tax,'cac:TaxSubtotal|cbc:TaxAmount',True))
                    newlst["SubTotalTaxCategoryIDItem_03"]=(getUniqueXML(tax,'cac:TaxSubtotal|cac:TaxCategory|cbc:ID',True))
                    newlst["SubTotalTaxPercentItem_03"]=(getUniqueXML(tax,'cac:TaxSubtotal|cac:TaxCategory|cbc:Percent',True))
                    newlst["SubTotalTaxExemptionReasonCodeItem_03"]=(getUniqueXML(tax,'cac:TaxSubtotal|cac:TaxCategory|cbc:TaxExemptionReasonCode',True))
                    newlst["SubTotalTaxIDItem_03"]=(getUniqueXML(tax,'cac:TaxSubtotal|cac:TaxCategory|cac:TaxScheme|cbc:ID',True))
                    newlst["SubTotalTaxNameItem_03"]=(getUniqueXML(tax,'cac:TaxSubtotal|cac:TaxCategory|cac:TaxScheme|cbc:Name',True))
                    newlst["SubTotalTaxTypeCodeItem_03"]=(getUniqueXML(tax,'cac:TaxSubtotal|cac:TaxCategory|cac:TaxScheme|cbc:TaxTypeCode',True))
                elif(countTaxes == 4):
                    newlst["SubTotalTaxAmountItem_04"]=(getUniqueXML(tax,'cac:TaxSubtotal|cbc:TaxAmount',True))
                    newlst["SubTotalTaxCategoryIDItem_04"]=(getUniqueXML(tax,'cac:TaxSubtotal|cac:TaxCategory|cbc:ID',True))
                    newlst["SubTotalTaxPercentItem_04"]=(getUniqueXML(tax,'cac:TaxSubtotal|cac:TaxCategory|cbc:Percent',True))
                    newlst["SubTotalTaxExemptionReasonCodeItem_04"]=(getUniqueXML(tax,'cac:TaxSubtotal|cac:TaxCategory|cbc:TaxExemptionReasonCode',True))
                    newlst["SubTotalTaxIDItem_04"]=(getUniqueXML(tax,'cac:TaxSubtotal|cac:TaxCategory|cac:TaxScheme|cbc:ID',True))
                    newlst["SubTotalTaxNameItem_04"]=(getUniqueXML(tax,'cac:TaxSubtotal|cac:TaxCategory|cac:TaxScheme|cbc:Name',True))
                    newlst["SubTotalTaxTypeCodeItem_04"]=(getUniqueXML(tax,'cac:TaxSubtotal|cac:TaxCategory|cac:TaxScheme|cbc:TaxTypeCode',True))
                countTaxes = countTaxes+1
            toReturn.append(newlst)
        return toReturn
    except Exception as e:
        return None


def getDataVersion_2dot0(inv,route):
    try:
        toReturn = []
        line = dict((k, []) for k in range(0))
        
        line["CodigoMall"]=None
        line["CodigoTienda"]=None
        line["RucEmisor"]=(getUniqueXML(inv,'cac:AccountingSupplierParty|cbc:CustomerAssignedAccountID',True))
        line["IdentificadorTerminal"]=None
        line["NumeroTerminal"]=None
        line["Serie"]=(getUniqueXML(inv,'cbc:ID',True)).split("-")[0]
        line["TipoTransaccion"]=(getUniqueXML(inv,'cbc:InvoiceTypeCode',True))
        line["NumeroTransaccion"]=(getUniqueXML(inv,'cbc:ID',True)).split("-")[1]
        line["Fecha"]=(getUniqueXML(inv,'cbc:IssueDate',True))
        line["Hora"]=(getUniqueXML(inv,'ext:UBLExtensions|ext:UBLExtension|ext:ExtensionContent|DatosAdicionales|DatoAdicional',True))
        line["Cajero"]=None
        line["Vendedor"]=None
        line["DNICliente"]=(getUniqueXML(inv,'cac:AccountingCustomerParty|cbc:CustomerAssignedAccountID',True))
        line["RUCCliente"]=(getUniqueXML(inv,'cac:AccountingCustomerParty|cbc:CustomerAssignedAccountID',True))
        line["NombreRazonSocialCliente"]=(getUniqueXML(inv,'cac:AccountingCustomerParty|cac:Party|cac:PartyLegalEntity|cbc:RegistrationName',True))
        #line["Direccion"]=(getUniqueXML(inv,'cac:AccountingCustomerParty|cac:Party|cac:PartyLegalEntity|cac:RegistrationAddress|cac:AddressLine|cbc:Line',True))
        line["Direccion"]=(getUniqueXML(inv,'cac:AccountingCustomerParty|cac:Party|cac:PartyLegalEntity|cac:RegistrationAddress|cac:AddressLine|cbc:Line',True)) #MODIFICADO POR BITZ
        line["Bonus"]=None
        line["Moneda"]=(getUniqueXML(inv,'cbc:DocumentCurrencyCode',True))
        line["MedioPago"]=None
        line["TotalValorVentaBruta"]=(getUniqueXML(inv,'cac:LegalMonetaryTotal|cbc:TaxInclusiveAmount',True))
        
        line["CargosDescuentosGlobal"]="-"
        line["MontoTotalIGV"]=(getUniqueXML(inv,'cac:TaxTotal|cbc:TaxAmount',True))
        line["TotalValorVentaNeta"]="-"
        
        #LINEAS DETALLE (CODIGO DE EXTRACCIÓN MAS ABAJO!)
        
        line["Filename"]=route
        line["CarpetaAzure"]=route
        line["Tienda"]=None
        line["UblType"]="Tag principal del XML"
        line["StatusOSE"]="-"
        line["UBLVersionID"]=getUniqueXML(inv,'cbc:UBLVersionID',True)
        line["CustomizationID"]=(getUniqueXML(inv,'cbc:CustomizationID',True))
        line["ID"]=(getUniqueXML(inv,'cbc:ID',True))
        line["Note"]=(getUniqueXML(inv,'cbc:Note',True))

        line["LineCountNumeric"]="-"
        line["TotalInvoiceAmount"]=(getUniqueXML(inv,'cac:LegalMonetaryTotal|cbc:PayableAmount',True))
        line["SupPartyId"]=(getUniqueXML(inv,'cac:AccountingSupplierParty|cbc:CustomerAssignedAccountID',True))
        line["SupPartyName"]=(getUniqueXML(inv,'cac:AccountingSupplierParty|cac:Party|cac:PartyName|cbc:Name',True))
        line["SupPartyRegName"]=(getUniqueXML(inv,'cac:AccountingSupplierParty|cac:Party|cac:PartyLegalEntity|cbc:RegistrationName',True))
        line["SupPartyRegAddrId"]=(getUniqueXML(inv,'cac:AccountingSupplierParty|cac:Party|cac:PostalAddress|cbc:ID',True))
        line["SupPartyRegAddrTypeCode"]=(getUniqueXML(inv,'cac:AccountingSupplierParty|cac:Party|cac:PostalAddress|cac:RegistrationAddress|cbc:AddressTypeCode',True))
        line["SupPartyRegAddrLine"]=(getUniqueXML(inv,'cac:AccountingSupplierParty|cac:Party|cac:PostalAddress|cbc:StreetName',True))
        line["SupPartyRegAddrCountry"]=(getUniqueXML(inv,'cac:AccountingSupplierParty|cac:Party|cac:PostalAddress|cac:Country|cbc:IdentificationCode',True))
        line["CusPartyId"]=(getUniqueXML(inv,'cac:AccountingCustomerParty|cbc:CustomerAssignedAccountID',True))
        line["CusPartyName"]="-"

        line["CusPartyRegName"]=(getUniqueXML(inv,'cac:AccountingCustomerParty|cac:Party|cac:PartyLegalEntity|cbc:RegistrationName',True))
        line["CusPartyRegAddrId"]="-"
        line["CusPartyRegAddrTypeCode"]="-"
        line["CusPartyRegAddrLine"]=(getUniqueXML(inv,'cac:AccountingCustomerParty|cac:Party|cac:PostalAddress|cbc:StreetName',True))
        line["CusPartyRegAddrCountry"]=(getUniqueXML(inv,'cac:AccountingCustomerParty|cac:Party|cac:PostalAddress|cac:Country|cbc:IdentificationCode',True))
        line["CpbtLines"]=len(inv.getElementsByTagName("cac:InvoiceLine"))
        line["CpbtTotal"]=line["TotalValorVentaNeta"]
        line["matchPos"]=None
        line["FechaCarga"]=None
        line["NombreMall"]=None
        line["NombreTienda"]=None
        
        line["ReferenceID"]=(getUniqueXML(inv,'cac:DiscrepancyResponse|cbc:ReferenceID',True))
        line["ResponseCode"]=(getUniqueXML(inv,'cac:DiscrepancyResponse|cbc:ResponseCode',True))
        line["Description"]=(getUniqueXML(inv,'cac:DiscrepancyResponse|cbc:Description',True))
        line["DocumentTypeCode"]=(getUniqueXML(inv,'cac:BillingReference|cac:InvoiceDocumentReference|cbc:DocumentTypeCode',True))
        line["Cus_party_subdiv_name"]="-"

        line["Cus_party_city_name"]=(getUniqueXML(inv,'cac:AccountingCustomerParty|cac:Party|cac:PostalAddress|cbc:CityName',True))
        line["Cus_party_country_subentity"]=(getUniqueXML(inv,'cac:AccountingCustomerParty|cac:Party|cac:PostalAddress|cbc:CountrySubentity',True))
        line["Cus_party_district"]=(getUniqueXML(inv,'cac:AccountingCustomerParty|cac:Party|cac:PostalAddress|cbc:District',True))
        line["Cus_party_email"]=(getUniqueXML(inv,'cac:AccountingCustomerParty|cac:Party|cac:Person|cac:Contact|cbc:ElectronicMail',True))
        line["Sup_party_subdiv_name"]=(getUniqueXML(inv,'cac:AccountingSupplierParty|cac:Party|cac:PostalAddress|cbc:CitySubdivisionName',True))
        line["Sup_party_city_name"]=(getUniqueXML(inv,'cac:AccountingSupplierParty|cac:Party|cac:PostalAddress|cbc:CityName',True))
        line["Sup_party_country_subentity"]=(getUniqueXML(inv,'cac:AccountingSupplierParty|cac:Party|cac:PostalAddress|cbc:CountrySubentity',True))
        line["Sup_party_district"]=(getUniqueXML(inv,'cac:AccountingSupplierParty|cac:Party|cac:PostalAddress|cbc:District',True))
        line["Sup_party_email"]="-"

        line["PayableRoundingAmount"]="-"
        line["Cus_party_person_id"]="-"
        line["Cus_party_person_email"]="-"
        line["SignatureID"]="-"
        line["ChargeIndicator"]="-"
        line["AllowanceChargeReasonCode"]="-"
        line["MultiplierFactorNumeric"]="-"
        line["AllowanceChargeAmount"]="-"
        line["BaseAmount"]="-" 

        line["SubTotalTaxAmount_01"]=None
        line["SubTotalTaxCategoryID_01"]=None
        line["SubTotalTaxID_01"]=None
        line["SubTotalTaxName_01"]=None
        line["SubTotalTaxTypeCode_01"]=None
        line["SubTotalTaxAmount_02"]=None
        line["SubTotalTaxCategoryID_02"]=None
        line["SubTotalTaxID_02"]=None
        line["SubTotalTaxName_02"]=None
        line["SubTotalTaxTypeCode_02"]=None
        line["SubTotalTaxAmount_03"]=None
        line["SubTotalTaxCategoryID_03"]=None
        line["SubTotalTaxID_03"]=None
        line["SubTotalTaxName_03"]=None
        line["SubTotalTaxTypeCode_03"]=None
        line["SubTotalTaxAmount_04"]=None
        line["SubTotalTaxCategoryID_04"]=None
        line["SubTotalTaxID_04"]=None
        line["SubTotalTaxName_04"]=None
        line["SubTotalTaxTypeCode_04"]=None
        countTaxes = 1
        invTaxes = inv.getElementsByTagName("cac:TaxTotal")
        for tax in invTaxes:
            if(countTaxes == 1):
                line["SubTotalTaxAmount_01"]=(getUniqueXML(tax,'cac:TaxSubtotal|cbc:TaxAmount',True))
                line["SubTotalTaxCategoryID_01"]=(getUniqueXML(tax,'cac:TaxSubtotal|cac:TaxCategory|cbc:ID',True))
                line["SubTotalTaxID_01"]=(getUniqueXML(tax,'cac:TaxSubtotal|cac:TaxCategory|cac:TaxScheme|cbc:ID',True))
                line["SubTotalTaxName_01"]=(getUniqueXML(tax,'cac:TaxSubtotal|cac:TaxCategory|cac:TaxScheme|cbc:Name',True))
                line["SubTotalTaxTypeCode_01"]=(getUniqueXML(tax,'cac:TaxSubtotal|cac:TaxCategory|cac:TaxScheme|cbc:TaxTypeCode',True))
            elif(countTaxes == 2):
                line["SubTotalTaxAmount_02"]=(getUniqueXML(tax,'cac:TaxSubtotal|cbc:TaxAmount',True))
                line["SubTotalTaxCategoryID_02"]=(getUniqueXML(tax,'cac:TaxSubtotal|cac:TaxCategory|cbc:ID',True))
                line["SubTotalTaxID_02"]=(getUniqueXML(tax,'cac:TaxSubtotal|cac:TaxCategory|cac:TaxScheme|cbc:ID',True))
                line["SubTotalTaxName_02"]=(getUniqueXML(tax,'cac:TaxSubtotal|cac:TaxCategory|cac:TaxScheme|cbc:Name',True))
                line["SubTotalTaxTypeCode_02"]=(getUniqueXML(tax,'cac:TaxSubtotal|cac:TaxCategory|cac:TaxScheme|cbc:TaxTypeCode',True))
            elif(countTaxes == 3):
                line["SubTotalTaxAmount_03"]=(getUniqueXML(tax,'cac:TaxSubtotal|cbc:TaxAmount',True))
                line["SubTotalTaxCategoryID_03"]=(getUniqueXML(tax,'cac:TaxSubtotal|cac:TaxCategory|cbc:ID',True))
                line["SubTotalTaxID_03"]=(getUniqueXML(tax,'cac:TaxSubtotal|cac:TaxCategory|cac:TaxScheme|cbc:ID',True))
                line["SubTotalTaxName_03"]=(getUniqueXML(tax,'cac:TaxSubtotal|cac:TaxCategory|cac:TaxScheme|cbc:Name',True))
                line["SubTotalTaxTypeCode_03"]=(getUniqueXML(tax,'cac:TaxSubtotal|cac:TaxCategory|cac:TaxScheme|cbc:TaxTypeCode',True))
            elif(countTaxes == 4):
                line["SubTotalTaxAmount_04"]=(getUniqueXML(tax,'cac:TaxSubtotal|cbc:TaxAmount',True))
                line["SubTotalTaxCategoryID_04"]=(getUniqueXML(tax,'cac:TaxSubtotal|cac:TaxCategory|cbc:ID',True))
                line["SubTotalTaxID_04"]=(getUniqueXML(tax,'cac:TaxSubtotal|cac:TaxCategory|cac:TaxScheme|cbc:ID',True))
                line["SubTotalTaxName_04"]=(getUniqueXML(tax,'cac:TaxSubtotal|cac:TaxCategory|cac:TaxScheme|cbc:Name',True))
                line["SubTotalTaxTypeCode_04"]=(getUniqueXML(tax,'cac:TaxSubtotal|cac:TaxCategory|cac:TaxScheme|cbc:TaxTypeCode',True))
            countTaxes = countTaxes+1
        
        invline = inv.getElementsByTagName("cac:InvoiceLine")
        for detail in invline:
            
            newlst = line.copy()
            
            newlst["NumeroOrdenItem"]=None
            newlst["CantidadUnidadesItem"]=None
            newlst["CodigoProducto"]=None
            newlst["DescripcionProducto"]=None
            newlst["PrecioVentaUnitarioItem"]=None
            newlst["CargoDescuentoItem"]=None
            newlst["PrecioTotalItem"]=None
            newlst["PrecioVentaUnitarioconIGVItem"]=None
            newlst["PriceTypeCode"]=None
            
            newlst["NumeroOrdenItem"]=(getUniqueXML(detail,'cbc:ID',True))
            newlst["CantidadUnidadesItem"]=(getUniqueXML(detail,'cbc:InvoicedQuantity',True))
            newlst["CodigoProducto"]=(getUniqueXML(detail,'cac:Item|cac:SellersItemIdentification|cbc:ID',True))
            newlst["DescripcionProducto"]=(getUniqueXML(detail,'cac:Item|cbc:Description',True))
            newlst["PrecioVentaUnitarioItem"]=(getUniqueXML(detail,'cac:PricingReference|cac:AlternativeConditionPrice|cbc:PriceAmount',True))
            newlst["CargoDescuentoItem"]=(getUniqueXML(detail,'cac:AllowanceCharge|cbc:Amount',True))
            newlst["PrecioTotalItem"]=(getUniqueXML(detail,'cbc:LineExtensionAmount',True))
            newlst["PrecioVentaUnitarioconIGVItem"]=(getUniqueXML(detail,'cac:PricingReference|cac:AlternativeConditionPrice|cbc:PriceAmount',True))
            newlst["PriceTypeCode"]=(getUniqueXML(detail,'cac:PricingReference|cac:AlternativeConditionPrice|cbc:PriceTypeCode',True))
            
            newlst["SubTotalTaxAmountItem_01"]=None
            newlst["SubTotalTaxCategoryIDItem_01"]=None
            newlst["SubTotalTaxPercentItem_01"]=None
            newlst["SubTotalTaxExemptionReasonCodeItem_01"]=None
            newlst["SubTotalTaxIDItem_01"]=None
            newlst["SubTotalTaxNameItem_01"]=None
            newlst["SubTotalTaxTypeCodeItem_01"]=None
            newlst["SubTotalTaxAmountItem_02"]=None
            newlst["SubTotalTaxCategoryIDItem_02"]=None
            newlst["SubTotalTaxPercentItem_02"]=None
            newlst["SubTotalTaxExemptionReasonCodeItem_02"]=None
            newlst["SubTotalTaxIDItem_02"]=None
            newlst["SubTotalTaxNameItem_02"]=None
            newlst["SubTotalTaxTypeCodeItem_02"]=None
            newlst["SubTotalTaxAmountItem_03"]=None
            newlst["SubTotalTaxCategoryIDItem_03"]=None
            newlst["SubTotalTaxPercentItem_03"]=None
            newlst["SubTotalTaxExemptionReasonCodeItem_03"]=None
            newlst["SubTotalTaxIDItem_03"]=None
            newlst["SubTotalTaxNameItem_03"]=None
            newlst["SubTotalTaxTypeCodeItem_03"]=None
            newlst["SubTotalTaxAmountItem_04"]=None
            newlst["SubTotalTaxCategoryIDItem_04"]=None
            newlst["SubTotalTaxPercentItem_04"]=None
            newlst["SubTotalTaxExemptionReasonCodeItem_04"]=None
            newlst["SubTotalTaxIDItem_04"]=None
            newlst["SubTotalTaxNameItem_04"]=None
            newlst["SubTotalTaxTypeCodeItem_04"]=None
            
            countTaxes = 1
            invTaxes = detail.getElementsByTagName("cac:TaxTotal")
            for tax in invTaxes:
                if(countTaxes == 1):
                    newlst["SubTotalTaxAmountItem_01"]=(getUniqueXML(tax,'cac:TaxSubtotal|cbc:TaxAmount',True))
                    newlst["SubTotalTaxCategoryIDItem_01"]=(getUniqueXML(tax,'cac:TaxSubtotal|cac:TaxCategory|cbc:ID',True))
                    newlst["SubTotalTaxPercentItem_01"]=(getUniqueXML(tax,'cac:TaxSubtotal|cac:TaxCategory|cbc:Percent',True))
                    newlst["SubTotalTaxExemptionReasonCodeItem_01"]=(getUniqueXML(tax,'cac:TaxSubtotal|cac:TaxCategory|cbc:TaxExemptionReasonCode',True))
                    newlst["SubTotalTaxIDItem_01"]=(getUniqueXML(tax,'cac:TaxSubtotal|cac:TaxCategory|cac:TaxScheme|cbc:ID',True))
                    newlst["SubTotalTaxNameItem_01"]=(getUniqueXML(tax,'cac:TaxSubtotal|cac:TaxCategory|cac:TaxScheme|cbc:Name',True))
                    newlst["SubTotalTaxTypeCodeItem_01"]=(getUniqueXML(tax,'cac:TaxSubtotal|cac:TaxCategory|cac:TaxScheme|cbc:TaxTypeCode',True))
                elif(countTaxes == 2):
                    newlst["SubTotalTaxAmountItem_02"]=(getUniqueXML(tax,'cac:TaxSubtotal|cbc:TaxAmount',True))
                    newlst["SubTotalTaxCategoryIDItem_02"]=(getUniqueXML(tax,'cac:TaxSubtotal|cac:TaxCategory|cbc:ID',True))
                    newlst["SubTotalTaxPercentItem_02"]=(getUniqueXML(tax,'cac:TaxSubtotal|cac:TaxCategory|cbc:Percent',True))
                    newlst["SubTotalTaxExemptionReasonCodeItem_02"]=(getUniqueXML(tax,'cac:TaxSubtotal|cac:TaxCategory|cbc:TaxExemptionReasonCode',True))
                    newlst["SubTotalTaxIDItem_02"]=(getUniqueXML(tax,'cac:TaxSubtotal|cac:TaxCategory|cac:TaxScheme|cbc:ID',True))
                    newlst["SubTotalTaxNameItem_02"]=(getUniqueXML(tax,'cac:TaxSubtotal|cac:TaxCategory|cac:TaxScheme|cbc:Name',True))
                    newlst["SubTotalTaxTypeCodeItem_02"]=(getUniqueXML(tax,'cac:TaxSubtotal|cac:TaxCategory|cac:TaxScheme|cbc:TaxTypeCode',True))
                elif(countTaxes == 3):
                    newlst["SubTotalTaxAmountItem_03"]=(getUniqueXML(tax,'cac:TaxSubtotal|cbc:TaxAmount',True))
                    newlst["SubTotalTaxCategoryIDItem_03"]=(getUniqueXML(tax,'cac:TaxSubtotal|cac:TaxCategory|cbc:ID',True))
                    newlst["SubTotalTaxPercentItem_03"]=(getUniqueXML(tax,'cac:TaxSubtotal|cac:TaxCategory|cbc:Percent',True))
                    newlst["SubTotalTaxExemptionReasonCodeItem_03"]=(getUniqueXML(tax,'cac:TaxSubtotal|cac:TaxCategory|cbc:TaxExemptionReasonCode',True))
                    newlst["SubTotalTaxIDItem_03"]=(getUniqueXML(tax,'cac:TaxSubtotal|cac:TaxCategory|cac:TaxScheme|cbc:ID',True))
                    newlst["SubTotalTaxNameItem_03"]=(getUniqueXML(tax,'cac:TaxSubtotal|cac:TaxCategory|cac:TaxScheme|cbc:Name',True))
                    newlst["SubTotalTaxTypeCodeItem_03"]=(getUniqueXML(tax,'cac:TaxSubtotal|cac:TaxCategory|cac:TaxScheme|cbc:TaxTypeCode',True))
                elif(countTaxes == 4):
                    newlst["SubTotalTaxAmountItem_04"]=(getUniqueXML(tax,'cac:TaxSubtotal|cbc:TaxAmount',True))
                    newlst["SubTotalTaxCategoryIDItem_04"]=(getUniqueXML(tax,'cac:TaxSubtotal|cac:TaxCategory|cbc:ID',True))
                    newlst["SubTotalTaxPercentItem_04"]=(getUniqueXML(tax,'cac:TaxSubtotal|cac:TaxCategory|cbc:Percent',True))
                    newlst["SubTotalTaxExemptionReasonCodeItem_04"]=(getUniqueXML(tax,'cac:TaxSubtotal|cac:TaxCategory|cbc:TaxExemptionReasonCode',True))
                    newlst["SubTotalTaxIDItem_04"]=(getUniqueXML(tax,'cac:TaxSubtotal|cac:TaxCategory|cac:TaxScheme|cbc:ID',True))
                    newlst["SubTotalTaxNameItem_04"]=(getUniqueXML(tax,'cac:TaxSubtotal|cac:TaxCategory|cac:TaxScheme|cbc:Name',True))
                    newlst["SubTotalTaxTypeCodeItem_04"]=(getUniqueXML(tax,'cac:TaxSubtotal|cac:TaxCategory|cac:TaxScheme|cbc:TaxTypeCode',True))
                countTaxes = countTaxes+1
            
            toReturn.append(newlst)
        return toReturn
    except Exception as e:
        return None
