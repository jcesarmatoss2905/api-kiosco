import os
import paramiko


def connectToSFTP(host,port,username,password):
    try:
        transport = paramiko.Transport((host,port))
        transport.connect(username = username, password = password)
        sftp = paramiko.SFTPClient.from_transport(transport)
        print("connection established successfully")
        return sftp
    except Exception as e:
        print(e)
        return None
        
def sftp_upload(host,port,username,password,local,remote,remove_local):
    local=local.replace("//", "/")
    remote=remote.replace("//", "/")
    transport = paramiko.Transport((host,port))
    transport.connect(username = username,password = password)
    sftp = paramiko.SFTPClient.from_transport(transport)
    try:
        if os.path.isdir(local):
            for f in os.listdir(local):
                full_local = local+"/"+f
                full_remote = remote+"/"+f
                full_local=full_local.replace("//", "/")
                full_remote=full_remote.replace("//", "/")
                if not os.path.isdir((os.path.join(full_local))):
                    sftp_upload(host,port,username,password,full_local,full_remote,remove_local)
                else:
                    sftp_upload(host,port,username,password,full_local+"/",full_remote+'/',remove_local)
        else:
            pathToVerify = "" 
            parts = remote.split('/')
            
            for part in parts:
                pathToVerify = pathToVerify+"/"+part
                try:
                    sftp.chdir(os.path.dirname(pathToVerify))  # Test if remote_path exists
                except IOError as e:
                    sftp.mkdir(os.path.dirname(pathToVerify)+'/')  # Create remote_path
                    sftp.chdir('.') # Ubicamos en Root
            
            print(local+ " ===> SUBIENDO OK ===>" + remote)
            sftp.put(local,remote)
    except Exception as e:
        print(e)
    transport.close()