from django.db import models

class ProductsModel(models.Model):
    class Meta:
        db_table = "products" #nombre de la tabla de sql
    
    Id = models.AutoField(primary_key=True)
    Name = models.CharField(max_length=255, null=True)
    Code = models.CharField(max_length=255, null=False)
    Price = models.IntegerField(max_length=255, null=True)
    Img = models.CharField(max_length=255, null=False)
    Is_active = models.BooleanField(null=True)