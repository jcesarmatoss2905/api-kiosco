from django.db import models

class UsersModel(models.Model):
    class Meta:
        db_table = "users"
    
    Id = models.AutoField(primary_key=True)
    Name = models.CharField(max_length=255, null=True)
    Email = models.CharField(max_length=255, null=True)
    Password = models.CharField(max_length=255, null=True)
    Is_active = models.BooleanField(max_length=255, null=False)