from django.db import models

class TypesModel(models.Model):
    class Meta:
        db_table = "types" #nombre de la tabla de sql
    
    Id = models.AutoField(primary_key=True)
    Type_group_id=models.IntegerField(null=False)
    Name = models.CharField(max_length=255, null=True)
    Code = models.CharField(max_length=255, null=False)