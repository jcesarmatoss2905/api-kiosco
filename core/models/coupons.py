from django.db import models

class CouponsModel(models.Model):
    class Meta:
        db_table = "coupons" #nombre de la tabla de sql
    
    Id = models.AutoField(primary_key=True)
    Title = models.CharField(max_length=255, null=True)
    Code = models.CharField(max_length=255, null=False)
    Name = models.CharField(max_length=255, null=True)
    Description = models.CharField(max_length=500, null=True)
    Url = models.CharField(max_length=500, null=True)
    Code_coupon = models.CharField(primary_key=True)
    Type_coupon_id = models.IntegerField(primary_key=True)
    Category_id = models.IntegerField(primary_key=True)
    Product_id=models.IntegerField(primary_key=True)
    Value = models.IntegerField(null=False)
    Img = models.CharField(max_length=255, null=False)
    Img_print = models.CharField(max_length=255, null=False)
    Is_percentage = models.BooleanField(max_length=255, null=False)
    Is_currencie = models.BooleanField(max_length=255, null=False)
    Menu_cupon = models.IntegerField(max_length=255, null=False)
    Expiration_date = models.CharField(max_length=255, null=True)
    Is_Active = models.BooleanField(null=True)