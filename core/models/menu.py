from django.db import models

class MenuModel(models.Model):
    class Meta:
        db_table = "menus" #nombre de la tabla de sql
    
    Id = models.AutoField(primary_key=True)
    Title = models.CharField(max_length=255, null=True)
    Name = models.CharField(max_length=255, null=True)
    Description = models.CharField(max_length=500, null=True)
    #Created_at = models.DateTimeField(null=True)
    #Updated_at = models.DateTimeField(null=True)
    Img = models.CharField(max_length=255, null=False)
    Code = models.CharField(max_length=255, null=False)

    
    """def insert(self, Title, Names, Description,Image,Codes):
        object = self.model(
            title = Title,
            names = Names,
            description = Description,
            image = Image,
            codes = Codes
        )
        object.save(using=self.db)
        return object"""
    
    """def __str__(self):
        return f'{self.Title} {self.Names} - {self.Description}'
    
    def insert(self, Title, Names, Description, Created_at, Updated_at, Image,Codes):
        object = self.model(
            Title = Title,
            Names = Names,
            Description = Description,
            Created_at = Created_at,
            Updated_at = Updated_at,
            Image = Image,
            Codes = Codes
        )
        object.save(using=self.db)
        return object
        --------------------------------
        def ExecuteUpdateComprobantes(self):
        from django.db import connection
        cursor = connection.cursor()
        try:
            cursor.execute('EXEC [dbo].[USP_ACTUALIZAR_DATOS_COMPROBANTES]')
            result_set = cursor.fetchall()
        finally:
            cursor.close()
        return result_set[0][0] #COLUMNA 0 - FILA 0"""