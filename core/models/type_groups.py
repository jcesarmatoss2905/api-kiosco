from django.db import models

class Type_groupsModel(models.Model):
    class Meta:
        db_table = "type_groups" #nombre de la tabla de sql
    
    Id = models.AutoField(primary_key=True)
    Name = models.CharField(max_length=255, null=True)
    Code = models.CharField(max_length=255, null=False)