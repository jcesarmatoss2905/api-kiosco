from django.db import models

class Customer_has_menusModel(models.Model):
    class Meta:
        db_table = "customer_has_menus" #nombre de la tabla de sql
    
    Id = models.AutoField(primary_key=True)
    Customer_id = models.IntegerField(primary_key=True)
    Menu_id = models.IntegerField(primary_key=True)
    Is_active = models.BooleanField(null=True)
    