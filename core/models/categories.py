from django.db import models

class CategoriesModel(models.Model):
    class Meta:
        db_table = "categories" #nombre de la tabla de sql
    
    Id = models.AutoField(primary_key=True)
    Name = models.CharField(max_length=255, null=True)
    Code = models.CharField(max_length=255, null=False)
    Description = models.CharField(max_length=500, null=True)
    Code = models.CharField(max_length=255, null=True)
    Is_Active = models.BooleanField(null=False)