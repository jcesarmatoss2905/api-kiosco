from django.db import models

class Customer_has_couponsModel(models.Model):
    class Meta:
        db_table = "customer_has_coupons" #nombre de la tabla de sql
    
    Id = models.AutoField(primary_key=True)
    Coupon_id = models.IntegerField(primary_key=True)
    Customer_id = models.IntegerField(primary_key=True)
    Precision = models.IntegerField(max_length=255, null=True)
    Status_print = models.BooleanField(null=True)
    Is_comes_from_trigger = models.BooleanField(null=True)