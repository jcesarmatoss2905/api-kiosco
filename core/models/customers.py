from django.db import models

class CustomersModel(models.Model):
    class Meta:
        db_table = "customers"
    
    Id = models.AutoField(primary_key=True)
    Name = models.CharField(max_length=255, null=True)
    Lastname = models.CharField(max_length=255, null=True)
    Email = models.CharField(max_length=255, null=True)
    Type_document_id = models.IntegerField(primary_key=True)
    Number_document = models.CharField(max_length=255, null=False)
    Phone = models.CharField(max_length=255, null=False)
    Status = models.BooleanField(null=True)
    Number_coupon = models.IntegerField(max_length=255, null=False)