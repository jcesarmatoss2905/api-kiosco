from rest_framework import serializers
from core.models.products import ProductsModel

class ProductsSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProductsModel
        fields = '__all__'