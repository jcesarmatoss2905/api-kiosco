from rest_framework import serializers
from core.models.coupons import CouponsModel

class CouponsSerializer(serializers.ModelSerializer):
    class Meta:
        model = CouponsModel
        fields = '__all__'