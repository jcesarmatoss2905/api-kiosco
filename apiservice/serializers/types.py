from rest_framework import serializers
from core.models.types import TypesModel

class TypesSerializer(serializers.ModelSerializer):
    class Meta:
        model = TypesModel
        fields = '__all__'