from rest_framework import serializers
from core.models.customer_has_menus import Customer_has_menusModel

class Customer_has_menuSerializer(serializers.ModelSerializer):
    class Meta:
        model = Customer_has_menusModel
        fields = '__all__'