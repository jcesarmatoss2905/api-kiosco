from rest_framework import serializers
from core.models.customer_has_coupons import Customer_has_couponsModel

class Customer_has_couponsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Customer_has_couponsModel
        fields = '__all__'