from rest_framework import serializers
from core.models.menu import MenuModel

class MenuSerializer(serializers.ModelSerializer):
    class Meta:
        model = MenuModel
        fields = '__all__'