from rest_framework import serializers
from core.models.users import UsersModel

class UsersSerializer(serializers.ModelSerializer):
    class Meta:
        model = UsersModel
        fields = '__all__'