from rest_framework import serializers
from core.models.type_groups import Type_groupsModel

class Type_groupsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Type_groupsModel
        fields = '__all__'