from rest_framework import serializers
from core.models.categories import CategoriesModel

class CategoriesSerializer(serializers.ModelSerializer):
    class Meta:
        model = CategoriesModel
        fields = '__all__'