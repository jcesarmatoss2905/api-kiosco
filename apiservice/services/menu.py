from rest_framework.response import Response
from rest_framework import status, generics
from core.models.menu import MenuModel
from apiservice.serializers.menu import *
from rest_framework.decorators import action
from rest_framework import viewsets
from annoying.functions import get_object_or_None
from core.libs import constants
from rest_framework.parsers import JSONParser
import logging
import jwt
class MenuService(viewsets.GenericViewSet):
    serializer_class = MenuSerializer
    queryset = MenuModel.objects.all()
    parser_classes = [JSONParser]

    @action(detail=False, methods=['get'])
    def get_all_menu (self,request):
        list = MenuModel.objects.all()
        serializer = self.serializer_class(list,many=True)
        Listadatos=serializer.data
        return Response(
        Listadatos
        ,content_type="application/json")
    
    @action(detail=False, methods=['post'])
    def insert (self,request):
        serializer = self.serializer_class(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response({constants.get_api_response_status(): constants.get_api_success()
                             , constants.get_api_response_data(): serializer.data
                             , constants.get_api_response_message(): constants.get_api_success()}
                            ,content_type="application/json")
        else:
            return Response({constants.get_api_response_status(): constants.get_api_error()
                             , constants.get_api_response_message(): serializer.errors}
                            ,content_type="application/json")
    
    @action(detail=False, methods=['post'])
    def save_menu (self,request):
        if "Title" and "Name" and "Description" and "Img" and "Code" in request.data :
            if request.data["menu_id"] == "-1" :
                menu = MenuModel(
                    title = request.data["title"],
                    name = request.data["name"],
                    description = request.data["description"],
                    img = request.data["img"],
                    code = request.data["code"]
                )
                menu.save()
            else:#Actualizar registro
                menu = MenuModel.objects.get(Id=int(request.data["menu_id"]))
                menu.title = request.data["title"]
                menu.name = request.data['name']
                menu.description = request.data['description']
                menu.img = request.data['img']
                menu.code = request.data['code']
                menu.save()
        return Response({
            constants.get_api_response_status(): constants.get_api_success()
            , constants.get_api_response_data(): [] #Retornar el objeto
            , constants.get_api_response_message(): constants.get_api_success()
        },content_type="application/json")
    
    @action(detail=False,methods=['post'])
    def get_by_id(self, request):
        if "menu_id" in request.data:
            menu = MenuModel.objects.all().filter(Id=request.data["menu_id"]).first()
            serializer = self.serializer_class(menu,many=False)
        return Response({
                constants.get_api_response_status(): constants.get_api_success()
                ,constants.get_api_response_data(): serializer.data
                ,constants.get_api_response_message(): constants.get_api_success()
        },content_type='application/json')
    
    @action(detail=False,methods=['post'])
    def delete_menus (self,request):
        if "menu_id" in request.data:
            menu = MenuModel.objects.get(Id=int(request.data["menu_id"]))
            menu.delete()
        return Response({
                constants.get_api_response_status(): constants.get_api_success()
                , constants.get_api_response_data(): []
                , constants.get_api_response_message(): constants.get_api_success()
        },content_type='application/json')
    