from rest_framework.response import Response
from rest_framework import status, generics
from core.models.users import UsersModel
from apiservice.serializers.users import *
from rest_framework.decorators import action
from rest_framework import viewsets
from annoying.functions import get_object_or_None
from core.libs import constants
from rest_framework.parsers import JSONParser
import logging
import jwt
class UsersService(viewsets.GenericViewSet):
    serializer_class = UsersSerializer
    queryset = UsersModel.objects.all()
    parser_classes = [JSONParser]

    @action(detail=False, methods=['get'])
    def get_all_users (self,request):
        list = UsersModel.objects.all()
        serializer = self.serializer_class(list,many=True)
        Listadatos=serializer.data
        return Response(
        Listadatos
        ,content_type="application/json")
    
    @action(detail=False, methods=['post'])
    def insert (self,request):
        serializer = self.serializer_class(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response({constants.get_api_response_status(): constants.get_api_success()
                             , constants.get_api_response_data(): serializer.data
                             , constants.get_api_response_message(): constants.get_api_success()}
                            ,content_type="application/json")
        else:
            return Response({constants.get_api_response_status(): constants.get_api_error()
                             , constants.get_api_response_message(): serializer.errors}
                            ,content_type="application/json")
    @action(detail=False,methods=['post'])
    def delete_users (self,request):
        if "users_id" in request.data:
            menu = UsersModel.objects.get(Id=int(request.data["users_id"]))
            menu.delete()
        return Response({
                constants.get_api_response_status(): constants.get_api_success()
                , constants.get_api_response_data(): []
                , constants.get_api_response_message(): constants.get_api_success()
        },content_type='application/json')
    
    @action(detail=False,methods=['post'])
    def get_by_id(self, request):
        if "users_id" in request.data:
            users = UsersModel.objects.all().filter(Id=request.data["users_id"]).first()
            serializer = self.serializer_class(users,many=False)
        return Response({
                constants.get_api_response_status(): constants.get_api_success()
                ,constants.get_api_response_data(): serializer.data
                ,constants.get_api_response_message(): constants.get_api_success()
        },content_type='application/json')
    
    @action(detail=False, methods=['post'])
    def save_users (self,request):
        if "user_id" in request.data:
            users = UsersModel.objects.get(Id=int(request.data["user_id"]))
            users.Name=request.data["Name"]
            users.Email=request.data["Email"]
            users.Password=request.data["Password"]
            users.Is_active=request.data["Is_active"]
            users.save()
        return Response({
            constants.get_api_response_status(): constants.get_api_success()
            , constants.get_api_response_data(): [] #Retornar el objeto
            , constants.get_api_response_message(): constants.get_api_success()
        },content_type="application/json")