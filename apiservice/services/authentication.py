from rest_framework.response import Response
from rest_framework import status, generics
from core.models.users import UsersModel
from apiservice.serializers.users import *
from rest_framework.decorators import action
from rest_framework import viewsets
from annoying.functions import get_object_or_None
from core.libs import constants
from rest_framework.parsers import JSONParser
import jwt

class AuthenticationService(viewsets.GenericViewSet):
    serializer_class = UsersSerializer
    queryset = UsersModel.objects.all()
    parser_classes = [JSONParser]
    @action(detail=False, methods=['post'])
    def auth(self, request):
        params=self.request.query_params
        userid=params.get("user_id")
        useremail=params.get("user_email")
        payload = {
            'user_id': userid,
            'email': useremail
        #'clave_secreta' : request.user_password
        } 
        token = jwt.encode(payload,"", algorithm='HS256')
       
        return Response({
            token
        }
        ,content_type="application/json")