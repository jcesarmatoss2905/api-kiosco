from rest_framework.response import Response
from rest_framework import status, generics
from core.models.customer_has_coupons import Customer_has_couponsModel
from apiservice.serializers.customer_has_coupons import *
from rest_framework.decorators import action
from rest_framework import viewsets
from annoying.functions import get_object_or_None
from core.libs import constants
from rest_framework.parsers import JSONParser
import logging
import jwt
class Customer_has_couponsService(viewsets.GenericViewSet):
    serializer_class = Customer_has_couponsSerializer
    queryset = Customer_has_couponsModel.objects.all()
    parser_classes = [JSONParser]

    @action(detail=False, methods=['get'])
    def get_all_customer_has_coupons (self,request):
        list = Customer_has_couponsModel.objects.all()
        serializer = self.serializer_class(list,many=True)
        Listadatos=serializer.data
        return Response(
        Listadatos
        ,content_type="application/json")
    
    @action(detail=False, methods=['post'])
    def insert (self,request):
        serializer = self.serializer_class(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response({constants.get_api_response_status(): constants.get_api_success()
                             , constants.get_api_response_data(): serializer.data
                             , constants.get_api_response_message(): constants.get_api_success()}
                            ,content_type="application/json")
        else:
            return Response({constants.get_api_response_status(): constants.get_api_error()
                             , constants.get_api_response_message(): serializer.errors}
                            ,content_type="application/json")
        
    @action(detail=False,methods=['post'])
    def delete_customer_has_coupons (self,request):
        if "customer_has_coupons_id" in request.data:
            menu = Customer_has_couponsModel.objects.get(Id=int(request.data["customer_has_coupons_id"]))
            menu.delete()
        return Response({
                constants.get_api_response_status(): constants.get_api_success()
                , constants.get_api_response_data(): []
                , constants.get_api_response_message(): constants.get_api_success()
        },content_type='application/json')
    
    @action(detail=False,methods=['post'])
    def get_by_id(self, request):
        if "customer_has_coupons_id" in request.data:
            customer_has_coupons = Customer_has_couponsModel.objects.all().filter(Id=request.data["customer_has_coupons_id"]).first()
            serializer = self.serializer_class(customer_has_coupons,many=False)
        return Response({
                constants.get_api_response_status(): constants.get_api_success()
                ,constants.get_api_response_data(): serializer.data
                ,constants.get_api_response_message(): constants.get_api_success()
        },content_type='application/json')
    
    