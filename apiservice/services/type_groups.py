from rest_framework.response import Response
from rest_framework import status, generics
from core.models.type_groups import Type_groupsModel
from apiservice.serializers.type_groups import *
from rest_framework.decorators import action
from rest_framework import viewsets
from annoying.functions import get_object_or_None
from core.libs import constants
from rest_framework.parsers import JSONParser
import logging
import jwt
class Type_groupsService(viewsets.GenericViewSet):
    serializer_class = Type_groupsSerializer
    queryset = Type_groupsModel.objects.all()
    parser_classes = [JSONParser]

    @action(detail=False, methods=['get'])
    def get_all_type_groups (self,request):
        list = Type_groupsModel.objects.all()
        serializer = self.serializer_class(list,many=True)
        Listadatos=serializer.data
        return Response(
        Listadatos
        ,content_type="application/json")
    
    @action(detail=False, methods=['post'])
    def insert (self,request):
        serializer = self.serializer_class(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response({constants.get_api_response_status(): constants.get_api_success()
                             , constants.get_api_response_data(): serializer.data
                             , constants.get_api_response_message(): constants.get_api_success()}
                            ,content_type="application/json")
        else:
            return Response({constants.get_api_response_status(): constants.get_api_error()
                             , constants.get_api_response_message(): serializer.errors}
                            ,content_type="application/json")
    @action(detail=False,methods=['post'])
    def delete_type_groups (self,request):
        if "type_groups_id" in request.data:
            menu = Type_groupsModel.objects.get(Id=int(request.data["type_groups_id"]))
            menu.delete()
        return Response({
                constants.get_api_response_status(): constants.get_api_success()
                , constants.get_api_response_data(): []
                , constants.get_api_response_message(): constants.get_api_success()
        },content_type='application/json')
    
    @action(detail=False,methods=['post'])
    def get_by_id(self, request):
        if "type_groups_id" in request.data:
            type_groups = Type_groupsModel.objects.all().filter(Id=request.data["type_groups_id"]).first()
            serializer = self.serializer_class(type_groups,many=False)
        return Response({
                constants.get_api_response_status(): constants.get_api_success()
                ,constants.get_api_response_data(): serializer.data
                ,constants.get_api_response_message(): constants.get_api_success()
        },content_type='application/json')
