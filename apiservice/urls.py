
from django.urls import path,include
from rest_framework import routers
from apiservice.services.authentication import AuthenticationService
from apiservice.services.menu import MenuService
from apiservice.services.categories import CategoriesService
from apiservice.services.coupons import CouponsService
from apiservice.services.customer_has_coupons import Customer_has_couponsService
from apiservice.services.customer_has_menus import Customer_has_menusService
from apiservice.services.customers import CustomerService
from apiservice.services.products import ProductsService
from apiservice.services.type_groups import Type_groupsService
from apiservice.services.types import TypesService
from apiservice.services.users import UsersService
router = routers.DefaultRouter()

router.register('authentication',AuthenticationService)
router.register('menus',MenuService)
router.register('categories',CategoriesService)
router.register('coupons',CouponsService)
router.register('customer_has_coupons',Customer_has_couponsService)
router.register('customer_has_menus',Customer_has_menusService)
router.register('products',ProductsService)
router.register('customers',CustomerService)
router.register('type_groups',Type_groupsService)
router.register('types',TypesService)
router.register('users',UsersService)

urlpatterns = router.urls

